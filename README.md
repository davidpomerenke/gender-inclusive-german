<h1><img alt="INCLUSIFY logo" height="40" src="./frontend/src/common/icons/inclusify-logo.svg"><img alt="Tech4Germany logo" height="45" src="./doc/images/tech4germany-logo.png" align="right"></h1>

[ [Live Demo](https://inclusify.tech.4germany.org/) 🚀 | [Project Info](https://tech.4germany.org/project/diversitaetssensible-sprache-bam/) 💁 | [Tech4Germany Fellowship][t4g] 🤓 ]

<a href="./doc/images/screenshot-inclusify-welcome-page.png"><img alt="INCLUSIFY start screen" height="200" src="./doc/images/screenshot-inclusify-welcome-page.png"></a>
<a href="./doc/images/screenshot-inclusify-with-results.png"><img alt="INCLUSIFY with results" height="200" src="./doc/images/screenshot-inclusify-with-results.png"></a>

## Technical Documentation

See [doc/index.md](./doc/index.md).

## License

This repository contains code and content we created ourselves, as well as content that we retrieved from other sources (some of it modified by us).

Our own source code and accompanying documentation in this repository are licensed under the [MIT license](./LICENSE). This applies to all files where no other license terms are included.

Files that are subject to other license terms are accompanied by appriopriate `LICENSE` files in the same or a higher directory.
