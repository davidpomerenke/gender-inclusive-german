import ast
from math import floor
import gzip
from itertools import chain
import json
from multiprocessing import Pool
import os
from random import random, seed
import re
import sys

import altair as alt
import pandas as pd
import spacy  # also requires `python -m spacy download de_core_news_sm`
from tqdm import tqdm

sys.path.insert(1, "..")

from matches import inter_i_matches, symbol_matches, all_matches

# whether to compute gender density per sentence for all documents
# (otherwise the senter is only run on sampled sentences)
DENSITY_PER_SENTENCE = False
NUM_SAMPLES = 1000

seed(1)


def norm(text: str) -> str:
    # normalize gender symbols to a common symbol
    text1 = re.sub(r"\b([A-ZÄÖÜ][a-zäöüß]{2,})I(n(nen)?)\b", r"\1*i\2", text)
    text2 = re.sub(
        r"\b([A-ZÄÖÜ][a-zäöüß]{2,})(\*|:|_|·|/-?)(in(nen)?)\b", r"\1*\3", text1
    )
    return text2


def ungender(text: str) -> str:
    # only used for generating suggestions
    # to speed up manual annotation
    text = re.sub(r"\b([A-ZÄÖÜ][a-zäöüß]{2,})I(n(nen)?)\b", r"\1", text)
    text = re.sub(r"\b([A-ZÄÖÜ][a-zäöüß]{2,})(\*|:|_|·|/-?)(in(nen)?)\b", r"\1", text)
    text = re.sub(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}in(nen)?) und ([A-ZÄÖÜ][a-zäöüß]{2,}\b)", r"\3", text
    )
    text = re.sub(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}) und ([A-ZÄÖÜ][a-zäöüß]{2,}in(nen)?\b)", r"\1", text
    )
    text = re.sub(r"(\b[A-ZÄÖÜ][a-zäöüß]{2,})enden?", r"\1er", text)
    return text


def test_ungender():
    assert (
        ungender("Die Mitarbeiter*innen mitarbeiten.") == "Die Mitarbeiter mitarbeiten."
    )
    assert (
        ungender("Die Mitarbeiter/-innen mitarbeiten.")
        == "Die Mitarbeiter mitarbeiten."
    )
    assert (
        ungender("Die MitarbeiterInnen mitarbeiten.") == "Die Mitarbeiter mitarbeiten."
    )
    assert (
        ungender("Die Mitarbeiterinnen und Mitarbeiter mitarbeiten.")
        == "Die Mitarbeiter mitarbeiten."
    )
    assert (
        ungender("Die Mitarbeiter und Mitarbeiterinnen mitarbeiten.")
        == "Die Mitarbeiter mitarbeiten."
    )
    assert ungender("Die Mitarbeitenden mitarbeiten.") == "Die Mitarbeiter mitarbeiten."
    assert (
        ungender("Die Mitarbeiter*innen und Mitarbeitenden")
        == "Die Mitarbeiter und Mitarbeiter"
    )


def senter(text: str) -> list[str]:
    return list(chain(*[[s.text for s in nlp(p).sents] for p in text.split("\n")]))


densities: list[float] = []


def augment_density(entry: str):
    d = ast.literal_eval(entry)
    if DENSITY_PER_SENTENCE:
        sents = senter(d["raw_content"])
        d["sents"] = sents
        density = int(len(all_matches(d["raw_content"])) / len(sents) * 1000) / 1000
    else:
        density = int(len(all_matches(d["raw_content"])) / d["length"] * 1000000) / 1000
    global densities
    densities.append(density)
    d["density"] = density
    return d


if __name__ == "__main__":
    nlp = spacy.load("de_core_news_sm", exclude=["parser"])
    nlp.enable_pipe("senter")
    entries_with_density = []
    print("reading files and augmenting docs with densities ...")
    for fn in tqdm(sorted(os.listdir("../dumps"))):
        if fn[0] == ".":
            continue
        with gzip.open("../dumps/" + fn, "rb") as f:
            entries = f.read().decode("utf-8").split("\n")
            proper_entries = [e for e in entries if len(e) > 5]
            entries_with_density += list(map(augment_density, proper_entries))
    print("sorting ...")
    sorted_entries = sorted(entries_with_density, key=lambda x: -x["density"])
    print("sampling ...")
    samples = []
    for entry in tqdm(sorted_entries[:NUM_SAMPLES]):
        if DENSITY_PER_SENTENCE:
            sents = entry["sents"]
        else:
            sents = senter(entry["raw_content"])
        r = floor(random() * len(sents))
        entry["y"] = norm(sents[r])
        entry["y_prev"] = norm(sents[r - 1]) if r > 0 else ""
        entry["x_suggestion"] = ungender(sents[r])
        entry["matches_suggestion"] = [ungender(s) for s in all_matches(sents[r])]
        entry["symbol_matches"] = len(
            symbol_matches(entry["raw_content"]) + inter_i_matches(entry["raw_content"])
        )
        entry["length"] = sum([len(s) for s in sents])
        del entry["raw_content"]
        del entry["title"]
        samples.append(entry)
    with open("benchmark_skeleton.json", "w") as f:
        json.dump(samples, f, ensure_ascii=False)
    with open("densities.json", "w") as f:
        densities = sorted(densities, key=lambda x: -x)
        print(
            "max, med, min density",
            densities[0],
            densities[int(NUM_SAMPLES / 2)],
            densities[NUM_SAMPLES],
        )
        density_data = {
            "id": list(range(len(densities))),
            "density": densities,
        }
        json.dump(density_data, f)

    df = pd.DataFrame(density_data)
    chart1 = alt.Chart(df).mark_bar().encode(alt.X("id:O"), alt.Y("density:Q"))
    chart1.save("chart1.html")
