from itertools import chain
import re

from neutral_words import neutral_match


def normalize(s: str) -> str:
    s = s.lower()
    s = re.sub(r"ä", "a", s)
    s = re.sub(r"ö", "o", s)
    s = re.sub(r"ü", "u", s)
    return s


def have_same_origin(a: str, b: str) -> bool:
    """
    Whether two words of possibly different gender are derived from the same root word.
    Examples:
        - Arbeiterinnen und Arbeiter
        - Ärztinnen und Ärzte
        - Beamte und Beamtinnen
    """
    length = min(len(a), len(b)) - 2
    return normalize(a[:length]) == normalize(b[:length])


def has_matches(text: str, n: int = 2) -> bool:
    if len(symbol_matches(text)) >= n:
        return True
    if len(inter_i_matches(text)) >= n:
        return True
    if len(dn_matches(text)) >= n:
        return True
    if len(neutral_matches_strict(text)) >= n:
        return True
    return False


def dn_matches(text: str) -> list[str]:
    matches1 = re.findall(
        r"((\b[A-ZÄÖÜ][a-zäöüß]{2,}in(nen)?) und ([A-ZÄÖÜ][a-zäöüß]{2,}\b))", text
    )
    matches2 = re.findall(
        r"((\b[A-ZÄÖÜ][a-zäöüß]{2,}) und ([A-ZÄÖÜ][a-zäöüß]{2,}in(nen)?\b))", text
    )
    return [
        *[m[0] for m in matches1 if have_same_origin(m[1], m[3])],
        *[m[0] for m in matches2 if have_same_origin(m[1], m[2])],
    ]


def symbol_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}(\*|:|_|·|/-?)in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def inter_i_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}In(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def star_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}\*in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def colon_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}:in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def underscore_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}_in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def interpunct_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}·in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def slash_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}/in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def slashminus_matches(text: str) -> list[str]:
    m = re.findall(
        r"(\b[A-ZÄÖÜ][a-zäöüß]{2,}/-in(nen)?\b)",
        text,
    )
    return [m[0] for m in m]


def neutral_matches_strict(text: str) -> list[str]:
    # for determining relevant documents
    m = re.findall(neutral_match, text)
    return [m[1] for m in m]


keys = [
    ("neutral", neutral_matches_strict),
    ("double_notation", dn_matches),
    ("internal_i", inter_i_matches),
    ("star", star_matches),
    ("colon", colon_matches),
    ("underscore", underscore_matches),
    ("slash", slash_matches),
    ("slashminus", slashminus_matches),
    ("interpunct", interpunct_matches),
]


def all_matches(text: str) -> list[str]:
    return list(chain(*[f(text) for _, f in keys]))
