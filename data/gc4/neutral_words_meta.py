with open("neutral_words.txt") as f1:
    with open("neutral_words.py", "w+") as f2:
        words = "|".join(l[:-1].capitalize() for l in f1.readlines())
        # match should not be at sentence start
        f2.write('neutral_match = r"([a-zäöüß]{3,} (' + words + ')n?\\b)"')
