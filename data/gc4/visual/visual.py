from ast import literal_eval
import gzip
import json
import math
import os
import re
from typing import Union

import pandas as pd
import altair as alt

import sys


def hist(l: list[str]) -> list[str]:
    d: dict[str, int] = dict()
    for e in l:
        if e in d:
            d[e] += 1
        else:
            d[e] = 1
    return [f"{k}: {v}" for k, v in sorted(d.items(), key=lambda x: -x[1])]


if not os.path.isfile("data.json"):
    sys.path.insert(1, "..")
    from matches import keys

    data: dict[str, list[Union[int, str]]] = {
        "date": [],
    }
    words: dict[str, list[str]] = dict()
    for key, _ in keys:
        data[key] = []
        words[key] = []
    for f in sorted(os.listdir("../dumps")):
        print(f[21:])
        if f[21:] != "filtered.tar.gz":
            continue
        match = re.findall(r"(20\d\d)-(\d\d)", f)[0]
        year = int(match[0])
        time_of_year = int(match[1]) / 52
        month = math.floor(time_of_year * 12)
        day = 1 + round((time_of_year - month / 12) * 365)
        date = f"{year}-{str(month).zfill(2)}-{str(day).zfill(2)}T00:00:00"
        print(match, date)
        count: dict[str, int] = dict()
        for key, _ in keys:
            count[key] = 0
        with gzip.open("../dumps/" + f, "rt") as file:
            for l in file.readlines():
                c = literal_eval(l)["raw_content"]
                for key, f in keys:
                    ws: list[str] = f(c)
                    words[key] += ws
                    count[key] += len(ws)
        for key, _ in keys:
            data[key].append(count[key])
        data["date"].append(date)

    with open("data.json", "w") as f:
        json.dump(data, f)

    if not os.path.isdir("words"):
        os.mkdir("words")
    for key, _ in keys:
        with open(f"words/{key}_unique.txt", "w") as f:
            f.write("\n".join(hist(words[key])))

with open("data.json", "r") as f:
    data = json.load(f)

df = pd.DataFrame(data)
df = df.melt(
    id_vars=["date"],
)


# Chart 1: General trend of gendering
# FIXME
# This would need to be relative to the total text amount in GC4,
# we don't have that in here at the moment.
# <- Saving them in `file_sizes.json` now.

chart1 = (
    alt.Chart(df)
    .mark_line()
    .encode(alt.X("date:T", timeUnit="yearmonth"), alt.Y("sum(value)"))
)

chart1.save("chart1.png", scale_factor=4.0)


# Chart 2: Trends of different gender styles

order = [
    "pair notation",
    "internal I",
    "neutral word",
    "*",
    "/",
    "/-",
    "_",
]

chart2 = (
    alt.Chart(df)
    .mark_area()
    .encode(
        alt.X("date:T", timeUnit="yearmonth", axis=alt.Axis(title=None, format="%Y")),
        alt.Y(
            "value:Q",
            stack="normalize",
            axis=alt.Axis(title="share", ticks=False, labels=False),
        ),
        alt.Color(
            "variable:N",
            sort=order,
        ),
    )
    .transform_filter(alt.datum.value >= 5)
    .properties(width=300, height=150)
)

chart2.save("chart2.png", scale_factor=4.0)


# Chart 3 Nice ranking visualization of the same data

df = pd.DataFrame(data)
df["date"] = pd.to_datetime(df["date"])
df = df.groupby(df["date"].dt.year).mean()
df["date"] = df.index
df = df.melt(id_vars="date")

chart3 = (
    alt.Chart(df)
    .mark_line(point=True)
    .encode(
        x=alt.X("date:N", axis=alt.Axis(title=None, labelAngle=0)),
        y="rank:O",
        color=alt.Color(
            "variable:N",
            sort=order,
        ),
    )
    .transform_window(
        rank="rank()",
        sort=[alt.SortField("value", order="descending")],
        groupby=["date"],
    )
    .transform_filter(alt.datum.value >= 2)
    .properties(width=300, height=150)
)

chart3.save("chart3.png", scale_factor=4.0)

(chart2 | chart3).configure_legend(title=None, labelFontSize=12, rowPadding=7.8).save(
    "charts2+3.png", scale_factor=4.0
)
