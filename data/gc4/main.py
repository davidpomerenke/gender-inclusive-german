# script adapted from: https://gist.github.com/Phil1108/e1821fec6eb746edc8e04ef5f76d23f1
# assumes the tar.gz files in directory `data_head_url`

from ast import literal_eval
import json
import os

from tqdm import tqdm

from matches import has_matches


in_dir = "/Volumes/MuchSpace/GC4/head_0"
out_dir = "dumps"


def process_file(filename: str, filename_out: str) -> None:
    print(f"reading {filename} ...")
    with open(os.path.join(in_dir, filename), "r") as file_in:
        with open("in_progress", "w") as file_out:
            i = 0
            file_out.write("[\n")
            while (file_content := file_in.read(10**9)) != "":
                print(f"read gigabyte nr {i} ...")
                i += 1
                print("splitting ...")
                a = file_content.split("{'url'")
                file_sizes[filename] = len(a)
                print("filtering and saving ...")
                # ignore broken documents at gigabyte boundaries
                # and take a sample
                for item in tqdm(a[1:-1:10]):
                    if len(item) > 10:
                        part = "{'url'" + item
                        ev = literal_eval(part)
                        if ev["language_score"] > 0.98:
                            if has_matches(ev["raw_content"]):
                                file_out.write(json.dumps(part) + "\n")
            file_out.write("]\n")
    print("moving to destination ...")
    os.rename("in_progress", filename_out)
    print(f"finished with {filename_out}!")


if __name__ == "__main__":
    i = 0
    file_sizes: dict[str, int] = dict()
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    for filename in tqdm(os.listdir(in_dir)):
        print("FILENAME", filename)
        if filename == "gc4_corpus_head_urls.txt":
            continue
        i += 1
        filename_out = f"{out_dir}/{filename[:-4]}_filtered.json"
        print(f"processing nr {i}: {filename_out} ...")
        if os.path.isfile(filename_out):
            print("File already exists.")
            continue
        process_file(filename, filename_out)

    print("Saving #docs / file ...")
    # FIXME does not work if the script is stopped and run again
    with open("file_sizes.json", "w") as f:
        json.dump(file_sizes, f)
    print("finished with everything!")

# fast filtering like:
# gunzip -c de_head_0000_2015-48_unfiltered.tar.gz | head -n 100000 | grep -E "pattern" | wc -l

# time without neutral matches: 07:07 (inner loop), 08:28 (outer loop)
# time with    neutral matches: 12:29 (inner loop), 13:44 (outer loop)
