from logging import warn
import re
from typing import Optional

from spacy.tokens import Token

from .custom_types import GenderingSymbol, GenderingType, LargeNlpStuff, Morph
from .morphy.morphy import inflect


def parse_morph(t: Token) -> Morph:
    """
    Parses the features of nouns and adjectives.
    """

    def get(a: str) -> Optional[str]:
        if not a in t.morph.to_dict():
            return None
        return t.morph.get(a)[0]

    case = get("Case")
    number = get("Number")
    # (Nominalized) adjectives are recognized as having no gender by Spacy.
    # But when we process them with Morphy, we need to assign an arbitrary gender.
    # TODO Refactor this when implementing the inflection of verbs.
    gender = get("Gender") or "Neut"

    if not case:
        warn(f"Cannot parse words without case: {t.text}; {t.morph.to_dict()}")
        case = "Nom"
    if not number:
        warn(f"Cannot parse words without number: {t.text}; {t.morph.to_dict()}")
        number = "Sing"

    return Morph(t.pos_, case, number, gender)


def add_gender_symbol(
    source: str,
    bad_word: str,
    inflected_good_root: str,
    gendering_type: GenderingType,
    prefix: str,
) -> list[str]:
    """
    Adds a gender symbol or double notation to a phrase.
    """

    def merge(a: str, b: str) -> str:
        if a == "":
            return b
        return a + b.lower()

    suggestions: list[str] = []
    if not source == "dereko":
        suggestions.append(merge(prefix, inflected_good_root))
    if source == "dereko":
        connector = "und" if re.match(r".*innen$", inflected_good_root) else "oder"
        suggestions.append(
            f"{merge(prefix, inflected_good_root)} {connector} {merge(prefix, bad_word)}"
        )
        # We only add the genderstar
        # The alternative symbols are added in the frontend
        # See frontend/src/common/language-tool-api/user-settings-language-mapping.ts
        # TODO change this and use the `gendering_type`
        if isinstance(gendering_type, GenderingSymbol):
            sym = gendering_type.gendering_symbol
            suggestions.append(
                merge(prefix, re.sub(r"(in(nen)?)$", rf"{sym}\1", inflected_good_root))
            )
    return suggestions


def inflect_root(
    root_of_bad_phrase: Token,
    suggestion: str,
    category_id: int,
    source: str,
    gendering_type: GenderingType,
    stuff: LargeNlpStuff,
    prefix: str,
) -> list[str]:
    """
    Adjusts the case (nominative, ...) and number (singular/plural) of the suggestion to those of the insensitive word.
    Currently we only do it for the root. We should also do it for all the words that depend on the root. (Specifically, those words that congruent to the root in case and number, that is, articles, personal pronouns, relative pronouns, etc.)
    Uses the morphological dictionary Morphy for that purpose (see morphy.py). Morphy does not consistently include different forms for nouns with definite article and nouns with indefinite article ("die Bediensteten" vs "Bedienstete"), so we do not take care of it either. In the future, we should find a workaround here (maybe it's always the suffix "e" vs "en"?), and then also take care of whether there is a definite or indefinite article.
    """
    phrase = next(stuff.pipelines.full(suggestion).sents)
    if len(roots := [w for w in phrase if w.dep_ == "ROOT"]) > 0:
        root_of_suggestion = roots[0]
    else:
        warn(f"No root found in suggestion {suggestion}.")
        return []
    if (
        root_of_bad_phrase.pos != root_of_suggestion.pos
        and not (
            root_of_bad_phrase.pos_ == "PROPN" and root_of_suggestion.pos_ == "NOUN"
        )
        and not (
            root_of_bad_phrase.pos_ == "NOUN" and root_of_suggestion.pos_ == "PROPN"
        )
    ):
        return []
    if not "Gender" in root_of_bad_phrase.morph.to_dict():
        # The word will not be corrected because its grammatical gender is neutral.
        # Perhaps it should still be corrected (it could contain a prefix that is not neutral.)
        return []
    # Try to inflect, otherwise use the uninflected word.
    # There can be mutiple matching forms in Morphy (albeit very rarely), so there can be multiple inflected versions of just one suggestion.
    m1 = parse_morph(root_of_bad_phrase)
    m2 = parse_morph(root_of_suggestion)
    inflected_roots_of_suggestions = inflect(
        root_of_suggestion.text,
        Morph(
            m2.pos,
            m1.case,
            "Plur" if category_id == 2 else m1.number,
            m2.gender,
        ),
        stuff.morphy,
    ) or [root_of_suggestion.text]
    suggestions_with_inflected_roots: list[str] = []
    for inflected_good_root in inflected_roots_of_suggestions:
        # TODO work with SPANs
        # For words that should be gendered with a symbol or with double notation, we only have the female form in the rule list. This is good, because we can inflect it easily as long as there are no symbols. After the inflection, it is now time to add the gender symbols, or the double notation:
        for inflected_root_with_gender_symbol in add_gender_symbol(
            source,
            root_of_bad_phrase._.text,  # type: ignore
            inflected_good_root,
            gendering_type,
            prefix,
        ):
            # For each inflected root, we assemble the other words around it. Here, we should also inflect dependent words, but currently we don't.
            suggestions_with_inflected_roots.append(
                "".join(
                    [
                        *[t.text_with_ws for t in phrase[: root_of_suggestion.i]],
                        inflected_root_with_gender_symbol,
                        *[t.text_with_ws for t in phrase[root_of_suggestion.i + 1 :]],
                    ]
                )
            )
    return suggestions_with_inflected_roots
