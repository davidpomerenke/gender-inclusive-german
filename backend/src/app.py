import logging
from os import path
from typing import Any
from flask import Flask, request, send_from_directory
from flask.wrappers import Response

from .api import form_to_gendering_type, TEXT_FIELD
from .matches import LargeNlpStuff, matches
from .morphy.morphy import load_morphy
from .prepare_list import load_rules
from .spacy_helpers import load_pipelines

# pyright: reportUntypedFunctionDecorator=false, reportUnknownMemberType=false

logging.basicConfig(level=logging.INFO)
app = Flask(__name__, static_folder=None)
stuff = LargeNlpStuff(load_pipelines(), load_morphy(), load_rules())


@app.route("/", defaults=dict(filename=None))
@app.route("/<path:filename>", methods=["GET"])
def index(filename: str) -> Response:
    filename = filename or "index.html"
    return send_from_directory(path.join("..", "static"), filename)


@app.route("/v2/check", methods=["POST"])
def serve_api() -> Any:
    if not TEXT_FIELD in request.form:
        return "No input text.", 500
    gendering_type = form_to_gendering_type(request.form)
    if not gendering_type:
        return "Gendering type or symbol invalid.", 500
    response = {
        "matches": matches(
            request.form["text"],
            stuff,
            gendering_type,
        )
    }
    return response, 200


if __name__ == "__main__":
    app.run(host="localhost", port=8081)
