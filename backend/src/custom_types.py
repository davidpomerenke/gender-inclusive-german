from typing import Callable, TypedDict, Optional, Union, Any
from dataclasses import dataclass

from spacy.tokens import Doc

# Gendering type


class Neutral:
    pass

    def __hash__(self):
        return hash("Neutral")

    def __eq__(self, other: Any):
        return hash(self) == hash(other)


class DoubleNotation:
    pass

    def __hash__(self):
        return hash("DoubleNotation")

    def __eq__(self, other: Any):
        return hash(self) == hash(other)


class InternalI:
    pass

    def __hash__(self):
        return hash("InternalI")

    def __eq__(self, other: Any):
        return hash(self) == hash(other)


@dataclass(frozen=True)
class GenderingSymbol:
    gendering_symbol: str

    def __hash__(self):
        return hash("GenderingSymbol {self.gendering_symbol}")

    def __eq__(self, other: Any):
        return hash(self) == hash(other)


GenderingType = Union[Neutral, DoubleNotation, InternalI, GenderingSymbol]


def to_gender_symbol(t: GenderingType) -> Optional[str]:
    if isinstance(t, GenderingSymbol):
        return t.gendering_symbol
    return None


def to_gendering_type(
    gendering_type: str, gendering_symbol: Optional[str]
) -> Optional[GenderingType]:
    mapping: dict[str, GenderingType] = {
        "neutral": Neutral(),
        "double-notation": DoubleNotation(),
        "internal-i": InternalI(),
    }
    if gendering_type in mapping:
        return mapping[gendering_type]
    elif (
        gendering_type == "gender-symbol"
        and gendering_symbol is not None
        and len(gendering_symbol) > 0
    ):
        return GenderingSymbol(gendering_symbol)
    return None


# List preparation:

# This type should better be a dataclass
UnprocessedRule = tuple[str, str, str, str]

# This type should better be a dataclass
ProcessedRule = tuple[str, str, str, str, str, str]


@dataclass
class Rule:
    insensitive_lemmas: str
    insensitive: str
    sensitive: str
    category_id: int
    source: str


# Other types:

Context = TypedDict(
    "Context",
    {
        "text": str,
        "offset": int,
        "length": int,
    },
)

Category = TypedDict(
    "Category",
    {
        "id": str,
        "name": str,
    },
)

Rule_ = TypedDict("Rule_", {"category": Category})

Replacement = TypedDict("Replacement", {"value": str})

Match = TypedDict(
    "Match",
    {
        "message": str,
        "shortMessage": str,
        "replacements": list[Replacement],
        "offset": int,
        "length": int,
        "context": Context,
        "rule": Rule_,
    },
)

SymbolList = list[tuple[int, str]]


@dataclass
class Morph:
    pos: str
    case: str
    number: str
    gender: str


MorphyDict = dict[str, list[tuple[str, str]]]


@dataclass
class Morphy:
    lemma_to_inflected: MorphyDict
    inflected_to_lemma: MorphyDict


Pipeline = Callable[[str], Doc]


@dataclass
class Pipelines:
    sents: Pipeline
    lemmas: Pipeline
    full: Pipeline


@dataclass
class LargeNlpStuff:
    """
    Helper class for passing the large and unhashable NLP and rule objects.
    They always have the same hash, so that they are effectively ignored for the caching.
    """

    pipelines: Pipelines
    morphy: Morphy
    rules: dict[str, list[Rule]]

    def __hash__(self):
        return hash("NLP config objects.")

    def __eq__(self, other: Any):
        return hash(self) == hash(other)
