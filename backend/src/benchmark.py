from functools import partial
from itertools import product
import json
import logging
import math
from random import randint
import time
from typing import Callable, Union

from .helpers import open_
from .matches import LargeNlpStuff, Match, matches
from .custom_types import GenderingSymbol, DoubleNotation
from .spacy_helpers import gender_symbol_wrapper

from tqdm import tqdm  # type: ignore

System = Callable[[str, bool], tuple[list[str], list[str]]]


def proportional_score(adjusted_rank: int, n: int = 5) -> float:
    return max(0, 1 - adjusted_rank / n)


def exponential_score(adjusted_rank: int, k: float = 0.6) -> float:
    return (1 + adjusted_rank) ** (-k)


def logistic_score(adjusted_rank: int, b: float = 0.4, v: float = 0.1) -> float:
    return 1 - 1 / ((1 + math.e ** (-b * adjusted_rank)) ** (1 / v))


def fmt(a: Union[int, float]):
    return round(a * 1000) / 1000


def benchmark(
    system: System,
):
    from .app import stuff

    with open_("../../data/gc4/benchmark/benchmark.json") as f:
        benchmark_data = json.load(f)
    pipelines = gender_symbol_wrapper(stuff.pipelines, GenderingSymbol("*"))
    tp: list[tuple[str, str]] = []
    tn: list[tuple[str, str]] = []
    fp: list[tuple[str, str]] = []
    fn: list[tuple[str, str]] = []
    in_any_list: list[int] = []
    in_n_best_list: list[int] = []
    in_1_best_list: list[int] = []
    scores_p: list[float] = []
    scores_e: list[float] = []
    scores_l: list[float] = []
    lemma_in_any_list: list[int] = []
    lemma_in_n_best_list: list[int] = []
    lemma_in_1_best_list: list[int] = []
    lemma_scores_p: list[float] = []
    lemma_scores_e: list[float] = []
    lemma_scores_l: list[float] = []
    logging.info("Running benchmark ...")
    for datum in tqdm(benchmark_data):  # type: ignore
        x: str = datum["x"]
        y: str = datum["y"]
        true_: list[str] = datum["matches"]
        symbol_matches: int = datum["symbol_matches"]
        positive, ranked_targets = system(x, symbol_matches > 0)
        mistake = False
        for token in pipelines.sents(x):
            word: str = token.text
            if (word in true_) and (word in positive):
                tp.append((word, x))
            if (word in true_) and (word not in positive):
                fn.append((word, x))
                mistake = True
            if (word not in true_) and (word in positive):
                fp.append((word, x))
                mistake = True
            if (word not in true_) and (word not in positive):
                tn.append((word, x))
        if x != y and not mistake:
            # calculated on the texts
            in_any = 0
            in_n_best = 0
            in_1_best = 0
            score_p = 0
            score_e = 0
            score_l = 0
            for i, y_ in enumerate(ranked_targets):
                if y_ == y:
                    in_any = 1
                    in_n_best = 1 if i < 5 else 0
                    in_1_best = 1 if i == 0 else 0
                    rank = i
                    adjusted_rank = rank ** (1 / len(true_))
                    score_p = proportional_score(adjusted_rank)
                    score_e = exponential_score(adjusted_rank)
                    score_l = logistic_score(adjusted_rank)
                    break
            in_any_list.append(in_any)
            in_n_best_list.append(in_n_best)
            in_1_best_list.append(in_1_best)
            scores_p.append(score_p)
            scores_e.append(score_e)
            scores_l.append(score_l)
            # calculated just on leamms TODO: refactor
            lemma_in_any = 0
            lemma_in_n_best = 0
            lemma_in_1_best = 0
            lemma_score_p = 0
            lemma_score_e = 0
            lemma_score_l = 0
            lemmas_y = frozenset(token.lemma_ for token in pipelines.sents(y))
            for i, y_ in enumerate(ranked_targets):
                lemmas_y_ = frozenset(token.lemma_ for token in pipelines.sents(y_))
                if lemmas_y_ == lemmas_y:
                    lemma_in_any = 1
                    lemma_in_n_best = 1 if i < 5 else 0
                    lemma_in_1_best = 1 if i == 0 else 0
                    rank = i
                    adjusted_rank = rank ** (1 / len(true_))
                    lemma_score_p = proportional_score(adjusted_rank)
                    lemma_score_e = exponential_score(adjusted_rank)
                    lemma_score_l = logistic_score(adjusted_rank)
                    break
            lemma_in_any_list.append(lemma_in_any)
            lemma_in_n_best_list.append(lemma_in_n_best)
            lemma_in_1_best_list.append(lemma_in_1_best)
            lemma_scores_p.append(lemma_score_p)
            lemma_scores_e.append(lemma_score_e)
            lemma_scores_l.append(lemma_score_l)

    recall = len(tp) / (len(tp) + len(fn))
    precision = len(tp) / pos if (pos := len(tp) + len(fp)) > 0 else 0
    # accuracy is misleading for this task and is not reported
    _accuracy = (len(tp) + len(tn)) / (len(tp) + len(fp) + len(tn) + len(fn))
    f1 = 2 * recall * precision / sum_ if (sum_ := recall + precision) > 0 else 0

    def avg(xs: Union[list[int], list[float]]) -> float:
        return sum(xs) / len(xs) if len(xs) > 0 else 0

    return {
        "tp": len(tp),
        "fn": len(fn),
        "fp": len(fp),
        "tn": len(tn),
        "recall": fmt(recall),
        "precision": fmt(precision),
        "f1": fmt(f1),
        "in any": fmt(avg(in_any_list)),
        "in n best": fmt(avg(in_n_best_list)),
        "in 1 best": fmt(avg(in_1_best_list)),
        "score (proportional)": fmt(avg(scores_p)),
        "score (exponential)": fmt(avg(scores_e)),
        "score (logistic)": fmt(avg(scores_l)),
        "lemma in any": fmt(avg(lemma_in_any_list)),
        "lemma in n best": fmt(avg(lemma_in_n_best_list)),
        "lemma in 1 best": fmt(avg(lemma_in_1_best_list)),
        "lemma score (proportional)": fmt(avg(lemma_scores_p)),
        "lemma score (exponential)": fmt(avg(lemma_scores_e)),
        "lemma score (logistic)": fmt(avg(lemma_scores_l)),
    }


def apply_matches(text: str, matches: list[Match]) -> list[str]:
    texts: list[str] = []
    rules = [[(match, r["value"]) for r in match["replacements"]] for match in matches]
    for rule in product(*rules):
        out = ""
        pos = 0
        for match, replacement in rule:
            out += text[pos : match["offset"]]
            out += replacement
            pos = match["offset"] + match["length"]
        out += text[pos:]
        texts.append(out)
    return texts


def inclusify_wrapper(sentence: str, use_gender_symbols: bool, splitparam: float = 0.6):
    from .app import stuff

    # Wraps Inclusify as a `System` for benchmarking
    gendering_type = GenderingSymbol("*") if use_gender_symbols else DoubleNotation()
    pipelines = gender_symbol_wrapper(stuff.pipelines, gendering_type)
    stuff = LargeNlpStuff(pipelines, stuff.morphy, stuff.rules)
    matches_ = matches(sentence, stuff, gendering_type, splitparam)
    labels = [
        sentence[match["offset"] : (match["offset"] + match["length"])]
        for match in matches_
    ]
    ranked_targets = apply_matches(sentence, matches_)
    return labels, ranked_targets


def run_benchmark():
    metrics = benchmark(inclusify_wrapper)
    with open_("../evaluation.json", "w") as f:
        json.dump(metrics, f, ensure_ascii=False, indent=4)


def run_benchmark_splitparam():
    splitparams = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1]
    recalls = []
    precisions = []
    f1s = []
    for s in tqdm(splitparams):
        wrapper = partial(inclusify_wrapper, splitparam=s)
        metrics = benchmark(wrapper)
        recalls.append(metrics["recall"])
        precisions.append(metrics["precision"])
        f1s.append(metrics["f1"])
    with open_("../evaluation_splitting.json", "w") as f:
        res = {
            "score": splitparams,
            "recall": recalls,
            "precision": precisions,
            "f1": f1s,
        }
        json.dump(res, f, ensure_ascii=False)


def splitting_visual():
    import pandas as pd
    import altair as alt

    with open_("../evaluation_splitting.json") as f:
        data = json.load(f)
    df = pd.DataFrame(data)
    df = df.melt(
        id_vars=["score"],
    )

    bars = (
        alt.Chart(df)
        .mark_bar()
        .encode(
            # alt.X("score:Q", title="min score for splitting"),
            alt.X("variable:N", title=None),
            alt.Y("value:Q", scale=alt.Scale(domain=[0.75, 1]), title=None),
            alt.Color("variable:N"),
            alt.Column("score:N", title="s₀"),
        )
    )

    bars.save("splitting.png", scale_factor=4.0)


def measure_time():
    from .app import stuff

    wiki = ""
    with open_("../data/dewiki-20220201-clean.txt") as f:
        while len(wiki) < 10_000_000:
            wiki += f.readline()
    sizes = []
    durations = []
    for _ in tqdm(range(10)):
        for size in [100, 250, 500, 750, 1000, 2500, 5000]:
            start = randint(0, 9_000_000)
            end = start + size
            text = wiki[start:end]
            t_start = time.time()
            matches(text, stuff)
            duration = time.time() - t_start
            sizes.append(size)
            durations.append(duration)
    with open_("../evaluation_time.json", "w") as f:
        metrics = {"size": sizes, "duration": durations}
        json.dump(metrics, f, ensure_ascii=False)


def time_visual():
    import pandas as pd
    import altair as alt

    with open_("../evaluation_time.json") as f:
        data = json.load(f)
    df = pd.DataFrame(data)

    line = alt.Chart(df).mark_line().encode(alt.X("size:Q"), alt.Y("mean(duration):Q"))
    dots = alt.Chart(df).mark_circle().encode(alt.X("size:Q"), alt.Y("duration:Q"))
    band = (
        alt.Chart(df)
        .mark_errorband(extent="ci")
        .encode(
            alt.X("size:Q", title="characters"), alt.Y("duration:Q", title="seconds")
        )
    )

    (line + dots + band).save("time.png", scale_factor=4.0)


if __name__ == "__main__":
    # run_benchmark()
    # run_benchmark_splitparam()
    splitting_visual()
    # measure_time()
    # time_visual()
