from .custom_types import Match, Replacement

SHORT_MESSAGE = 'Die Bezeichnung "{}" spricht nur männliche Leser an. Versuche alle Menschen anzusprechen.'

MESSAGE = "Es gibt verschiedene Ansätze für eine geschlechtergerechte Ausdrucksweise. Oft wird die Verwendung neutraler Begriffe bevorzugt. Darüber hinaus findet man auch oft Gendersymbole. Das Sternchen zum Beispiel insbesondere im Kontext der aktuellen Transgender- und Intersexualitätsdebatten: mit dem Sternchen sind mehr als zwei Geschlechter angesprochen."


def gender_match(text: str, replacements: list[str], offset: int, length: int) -> Match:
    """
    Takes the raw information about a match and returns the suitable format for the API.
    """
    replacement_values: list[Replacement] = [{"value": a} for a in replacements]

    return {
        "message": MESSAGE,
        "shortMessage": SHORT_MESSAGE.format(text),
        "replacements": replacement_values,
        "offset": offset,
        "length": length,
        "context": {
            "text": text,
            "offset": 0,
            "length": len(text),
        },
        "rule": {
            "category": {
                "id": "GENERISCHES_MASKULINUM",
                # The categories should match with the frontend
                # cf. frontend/src/common/rule-categories.ts
                "name": "Generisches Maskulinum",
            },
        },
    }
