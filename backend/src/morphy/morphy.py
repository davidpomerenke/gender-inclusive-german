from dataclasses import dataclass
import gzip
import logging
from pathlib import Path
import pickle
import re
from typing import Dict, List, Optional, Tuple, Set, Iterable

from compound_split import char_split  # type: ignore

from ..custom_types import Morph, Morphy


def load_morphy() -> Morphy:
    logging.info("Loading morphological dictionary ...")
    with gzip.open(Path(__file__).parent.absolute() / "morphy.pickle.gz") as f:
        a, b = pickle.load(f)
    logging.info("Finished loading morphological dictionary.")
    return Morphy(a, b)


def inflect(
    word: str,
    morph: Morph,
    morphy: Morphy,
    recursion: int = 0,
) -> Iterable[str]:
    """
    Adjust case, gender, and number of a word.
    This is a wrapper around the morphological dictionary Morphy.
    See the `LICENSE` file for more information on Morphy.
    """
    morph = to_morphy_format(morph)
    inflected: set[str] = set()
    if word not in morphy.inflected_to_lemma:
        # Words ending with "in" or "innen" (very common for gendered words) are not changed by inflections
        # except singular/plural changes, which we can handle manually
        if re.match(r".*in$", word) and morph.number == "PLU":
            return {word + "nen"}
        if re.match(r".*innen$", word) and morph.number == "SIN":
            return {re.sub(r"nen$", "", word)}
        if re.match(r".*in(nen)?$", word):
            return {word}
        if recursion > 0:
            return []
        for _, part_1, part_2 in char_split.split_compound(word)[:3]:
            inflected_part_2s = inflect(
                part_2,
                morph,
                morphy,
                recursion=recursion + 1,
            )
            for inflected_part_2 in inflected_part_2s:
                inflected.add(part_1 + inflected_part_2.lower())
        return set(inflected)
    for meaning in meanings(word, morphy.inflected_to_lemma):
        if meaning.pos == morph.pos:
            new_morph = ":".join([morph.pos, morph.case, morph.number, morph.gender])
            new_inflected = {
                inflected_
                for morph_, inflected_ in morphy.lemma_to_inflected[meaning.lemma]
                if re.match(new_morph, morph_) is not None
            }
            inflected = inflected.union(new_inflected)
    return set(inflected)


def to_morphy_format(m: Morph) -> Morph:
    """
    Spacy uses "Sing", "Masc" etc. but Morphy uses "SIN", "MAS", etc.
    Spacy uses English POS tags but Morphy uses German ones.
    This function converts the first to the latter.
    """
    if m.pos == "NOUN":
        m.pos = "SUB"
    ACCEPTED_POS = ["SUB"]
    if m.pos not in ACCEPTED_POS:
        logging.warning(f"POS must be one of {', '.join(ACCEPTED_POS)}, not {m.pos}.")

    def c(s: str) -> str:
        return s[:3].upper()

    return Morph(c(m.pos), c(m.case), c(m.number), c(m.gender))


@dataclass(frozen=True)
class Meaning:
    pos: str
    lemma: str


def meanings(
    word: str, inflected_to_lemma: Dict[str, List[Tuple[str, str]]]
) -> Iterable[Meaning]:
    meanings: Set[Meaning] = set()
    for morph, lemma in inflected_to_lemma[word]:
        if morph_ := parse_morph(morph):
            meanings.add(Meaning(morph_.pos, lemma))
    return meanings


def parse_morph(morph: str) -> Optional[Morph]:
    """
    Parse the features in Morphy.
    """
    morphs = morph.split(":")
    pos = morphs[0]
    if pos in ("SUB", "ADJ"):
        return Morph(
            morphs[0],
            morphs[1],
            morphs[2],
            morphs[3],
        )
    return None
