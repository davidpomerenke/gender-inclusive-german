import argparse
import pprint
from typing import Optional

from .custom_types import to_gendering_type, GenderingType
from .matches import LargeNlpStuff, matches


def matches_(
    text: str,
    gendering_type: GenderingType,
    concise: bool,
    stuff: Optional[LargeNlpStuff] = None,
    verbose: bool = False,
):
    if not stuff:
        from .spacy_helpers import load_pipelines
        from .morphy.morphy import load_morphy
        from .prepare_list import load_rules

        stuff = LargeNlpStuff(
            load_pipelines(),
            load_morphy(),
            load_rules(),
        )
    for m in matches(text, stuff, gendering_type):
        if concise:
            print(m["context"]["text"])
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint([a["value"] for a in m["replacements"]])
        else:
            pp = pprint.PrettyPrinter()
            pp.pprint(m)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Receive suggestions for more inclusive language."
    )
    parser.add_argument(
        "-s",
        "--style",
        dest="style",
        choices=["neutral", "double-notation", "internal-i", "gender-symbol"],
        default="double-notation",
        help="gender style",
    )
    parser.add_argument(
        "--symbol",
        dest="symbol",
        type=str,
        default="*",
        help="gender symbol",
    )
    parser.add_argument(
        "-f",
        "--file",
        dest="file",
        type=argparse.FileType(encoding="UTF-8"),
        help="Filename of plain text file to be checked, or '-' for Stdin.",
    )
    parser.add_argument(
        "-t",
        "--text",
        dest="text",
        type=str,
        help="Plain text to be checked.",
    )
    parser.add_argument(
        "--concise",
        action="store_const",
        dest="concise",
        const=True,
        default=False,
        help="More concise output.",
    )

    args = parser.parse_args()
    if not (args.file or args.text):
        print("Please specify a filename or text.")
        parser.print_help()
    assert (gendering_type := to_gendering_type(args.style, args.symbol)) is not None
    if args.file and args.text:
        print("Please specify either filename or text, not both.")
        parser.print_help()
    elif args.text:
        matches_(args.text, gendering_type, args.concise)
    elif args.file:
        matches_(args.file.read(), gendering_type, args.concise)
