import logging
from os import path
import re
from typing import List, Tuple, Callable, Iterable

import spacy
from spacy.language import Language
from spacy.tokens import Doc, Token, Span
import spacy_stanza  # type: ignore
import stanza  # type: ignore

from .custom_types import (
    SymbolList,
    GenderingType,
    to_gender_symbol,
    Pipeline,
    Pipelines,
)
from .helpers import optional_to_list

Doc.set_extension("text", default="")
Span.set_extension("words", default="")
Span.set_extension("text", default="")
Token.set_extension("idx", default=0)
Token.set_extension("lemma_", default="")
Token.set_extension("text", default="")
Token.set_extension("text_with_ws", default="")


def gendered_word_pattern(gender_symbols: List[str]) -> re.Pattern[str]:
    custom = "|".join(re.escape(s) for s in ["", *gender_symbols])
    p = r"([A-ZÄÖÜa-zäöüß][a-zäöüß][a-zäöüß]+)(\*|:|_|·|/-?|\.{custom})(in(nen)?)"
    return re.compile(p.format(custom=custom))


def remove_gender_symbols(
    text: str, gender_symbols: List[str]
) -> Tuple[str, SymbolList]:
    pattern = gendered_word_pattern(gender_symbols)
    symbols: SymbolList = []
    shift = 0
    for m in re.finditer(pattern, text):
        symbols.append((m.start() - shift + len(m[1]), m[2]))
        shift += len(m[2])
    return re.sub(pattern, r"\1\3", text), symbols


def restore_gender_symbols(text: str, symbols: SymbolList) -> str:
    new_text = ""
    i = 0
    last_i = 0
    for i, symbol in symbols:
        new_text += text[last_i:i] + symbol
        last_i = i
    return new_text + text[i:]


def restore_gender_symbols_token(
    token: Token, symbols: SymbolList, shift: int = 0
) -> Tuple[SymbolList, int]:
    token._.idx = token.idx + shift
    token._.text = token.text
    token._.text_with_ws = token.text_with_ws
    if len(symbols) > 0:
        i, _ = symbols[0]
        while token.idx < i < token.idx + len(token.text):
            (i, symbol), *symbols = symbols
            j = i - token.idx
            token._.text = token.text[:j] + symbol + token.text[j:]
            token._.text_with_ws = (
                token.text_with_ws[:j] + symbol + token.text_with_ws[j:]
            )
            shift += len(symbol)
            if len(symbols) == 0:
                break
            i, _ = symbols[0]
    return symbols, shift


def restore_gender_symbols_span(
    span: Span, symbols: SymbolList, shift: int = 0
) -> Tuple[SymbolList, int]:
    for token in span:
        symbols, shift = restore_gender_symbols_token(token, symbols, shift)
    span._.text = tokens_to_string(span)
    return symbols, shift


def restore_gender_symbols_doc(doc: Doc, symbols: SymbolList, shift: int = 0) -> None:
    for span in doc.sents:
        symbols, shift = restore_gender_symbols_span(span, symbols, shift)
    doc._.text = doc_to_string(doc)


def tokens_to_string(tokens: Iterable[Token]) -> str:
    return "".join([t._.text_with_ws for t in tokens])  # type: ignore


def doc_to_string(doc: Doc) -> str:
    return "".join([t._.text_with_ws for s in doc.sents for t in s])  # type: ignore


def augment_with_lemmas(nlp_spacy: Pipeline, nlp_stanza: Pipeline) -> Pipeline:
    def nlp_(s: str) -> Doc:
        doc = nlp_spacy(s)
        for token in doc:
            # TODO deal with multi word tokens from Stanza
            token._.lemma_ = nlp_stanza(token.text)[0].lemma_
        return doc

    return nlp_


@Language.component("set_custom_boundaries")  # type: ignore
def set_custom_boundaries(doc: Doc) -> Doc:
    """
    Fixes Spacy's sentence segmentation behaviour.
    Spacy does not recognize line breaks between sentences as space, probably due to normalized training data.
    We don't want to retrain it just for this issue, so here is a heuristic for introducing sentence breaks at line breaks.
    """
    for token in doc[2:]:
        punctuation = re.match(r"[.?!:;)\]*\"']", doc[token.i - 2].text)
        line_break = re.match(r"\n", doc[token.i - 1].text)
        long_line_break = re.match(r"\n\n+", doc[token.i - 1].text)
        if (punctuation and line_break) or long_line_break:
            token.is_sent_start = True
    return doc


def load_pipelines() -> Pipelines:
    THIS_DIR = path.dirname(path.realpath(__file__))
    MODEL_DIR = path.join(THIS_DIR, "../stanza_resources")
    LANG = "de"
    if not path.exists(path.join(THIS_DIR, MODEL_DIR, LANG)):
        logging.info(f"Downloading language models for language {LANG} ...")
        stanza.download(lang, model_dir=MODEL_DIR)  # type: ignore
        logging.info(f"Finished download for language {LANG}.")
    logging.info("Loading language models ...")
    spacy_sent = spacy.load("de_core_news_sm", exclude=["parser"])
    spacy_sent.enable_pipe("senter")
    spacy_sent.add_pipe("set_custom_boundaries", first=True)
    spacy_full = spacy.load("de_dep_news_trf")
    stanza_lemmas: Language = spacy_stanza.load_pipeline(  # type: ignore
        LANG,
        dir=MODEL_DIR,
        processors="tokenize,mwt,pos,lemma",
        logging_level=logging.getLevelName(logging.root.level),
    )
    logging.info("Finished loading language models.")
    spacy_full_with_stanza = augment_with_lemmas(spacy_full, stanza_lemmas)
    # TODO: for consistency, a wrapper similar to `gender_symbol_wrapper`
    # should be applied here, to make sure the custom attributes are all available!
    return Pipelines(spacy_sent, stanza_lemmas,  spacy_full_with_stanza)


def gender_symbol_wrapper_(
    nlp: Callable[[str], Doc], gendering_type: GenderingType
) -> Callable[[str], Doc]:
    gender_symbols = optional_to_list(to_gender_symbol(gendering_type))

    def nlp_(text: str) -> Doc:
        text_, symbols = remove_gender_symbols(text, gender_symbols)
        doc = nlp(text_)
        restore_gender_symbols_doc(doc, symbols)  # mutates `doc`
        return doc

    return nlp_


def gender_symbol_wrapper(nlp: Pipelines, gendering_type: GenderingType) -> Pipelines:
    return Pipelines(
        gender_symbol_wrapper_(nlp.sents, gendering_type),
        gender_symbol_wrapper_(nlp.lemmas, gendering_type),
        gender_symbol_wrapper_(nlp.full, gendering_type),
    )
