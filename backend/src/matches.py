from copy import copy, deepcopy
from functools import lru_cache
from itertools import chain
import re

from compound_split import char_split  # type: ignore[import]
from spacy.tokens import Token


from .custom_types import (
    Match,
    Rule,
    GenderingType,
    DoubleNotation,
    LargeNlpStuff,
)
from .grammar_transfer import inflect_root
from .messages import gender_match
from .spacy_helpers import gender_symbol_wrapper, tokens_to_string


def matches(
    text: str,
    stuff: LargeNlpStuff,
    gendering_type: GenderingType = DoubleNotation(),
    splitparam: float = 0.8,
) -> list[Match]:
    """
    Returns the rule matches (including information about the rule and the replacements) for a given text.
    This function may be called many times in a row for a text with only minor changes, because currently the frontend triggers a re-check whenever a replacement is accepted. Therefore, we partition the text into sentences with a cheap NLP model that only performs tokenization, and for each sentence we call a cached function. The more expensive NLP methods will thus only be triggered for sentences where the text has changed.
    """
    nlp = gender_symbol_wrapper(stuff.pipelines, gendering_type)
    doc = nlp.sents(text)
    matches_: list[Match] = []
    for sent in doc.sents:
        for match in matches_per_sentence(sent.text, gendering_type, stuff, splitparam):
            match_ = deepcopy(match)
            match_["offset"] += sent[0]._.idx  # type: ignore
            matches_.append(match_)
    return matches_


@lru_cache(maxsize=1000)
def matches_per_sentence(
    sentence_text: str,
    gendering_type: GenderingType,
    stuff: LargeNlpStuff,
    splitparam: float,
) -> list[Match]:
    """
    Collects the rule matches for each single word in the sentence and returns the combined results.
    A rule is always triggered by a single word (the root word of the insensitive phrase), thus this also deals with phrases consisting of multiple words.
    """
    doc = stuff.pipelines.full(sentence_text)
    matches: list[Match] = []
    for sent in doc.sents:
        matches += chain(
            *[
                matches_per_token(token, gendering_type, stuff, splitparam)
                for token in sent
            ]
        )
    return matches


def simplify_participles(phrase: str, root_of_bad_phrase: Token) -> str:
    """
    Simplify phrases like "bewerbende Personen" to "Bewerbende".
    """
    match = re.match(r"(^[a-zäöüß]+(ige|ene|te|nde)n?) (Person|Mensch|Firma)$", phrase)
    if root_of_bad_phrase.morph.get("Number") == ["PLU"] and match:
        return match[1].capitalize()
    return phrase


def have_different_genders(a: Token, b: Token) -> bool:
    if "Gender=Fem" in a.morph and "Gender=Masc" in b.morph:
        return True
    if "Gender=Masc" in a.morph and "Gender=Fem" in b.morph:
        return True
    return False


def normalize(s: str) -> str:
    s = s.lower()
    s = re.sub(r"ä", "a", s)
    s = re.sub(r"ö", "o", s)
    s = re.sub(r"ü", "u", s)
    return s


def have_same_origin(a: str, b: str) -> bool:
    """
    Whether two words of possibly different gender are derived from the same root word.
    Examples:
        - Arbeiterinnen und Arbeiter
        - Ärztinnen und Ärzte
        - Beamte und Beamtinnen
    """
    length = min(len(a), len(b) - 3)
    return normalize(a[:length]) == normalize(b[:length])


def is_part_of_double_notation(t: Token) -> bool:
    CONJUNCTIONS = [
        "und",
        "sowie",
        "oder",
        "beziehungsweise",
        "bzw.",
        "bzw",
    ]

    def back(a: int, b: int) -> int:
        return a + b - 1

    def forth(a: int, b: int) -> int:
        return a - b - 1

    for f in [back, forth]:
        i = f(t.i, 2)
        if (
            i >= 0
            and i < len(t.sent)
            and len(t.sent) >= f(t.i, 1)
            and t.sent[f(t.i, 1)].text in CONJUNCTIONS
        ):
            conjunct_t = t.sent[i]
            if have_different_genders(t, conjunct_t) and have_same_origin(
                t.text, conjunct_t.text
            ):
                return True
    return False


def is_applicable(rule: Rule, token: Token) -> bool:
    """
    Returns whether a rule is applicable given a word (the potential root of the insensitive phrase) and the sentence around the word.
    In the future, this should check whether the same dependency tree (with the same lemmas and the same connections between them) which is present in the rule is also present in the sentence. Right now, it just checks whether the root is the same and whether the other lemmas are also in the sentence. For the simple rules that we use for gendering, this is practically sufficient.
    """
    # Check if all lemmas from the rule are in the sentence
    # TODO there should be stricter checks, but for now this works
    bad_lemmas = rule.insensitive_lemmas.split(";")
    sentence_lemmas = [word._.lemma_ for word in token.sent]  # type: ignore
    if any(lemma not in sentence_lemmas for lemma in bad_lemmas):
        return False
    # If the root is singular but the rule is only applicable in plural
    # Cf. the documentation on rule lists
    if token.morph.get("Number") == ["Sing"] and rule.category_id == 2:
        return False
    # When the word is part of a double notation phrase with the female form of the same word, the rule has already been applied and is thus no longer applicable
    if is_part_of_double_notation(token):
        return False
    return True


def matches_from_subtokens(
    token: Token,
    gendering_type: GenderingType,
    stuff: LargeNlpStuff,
    splitparam: float,
    recursion: int,
) -> list[str]:
    suggestions: list[str] = []
    splits = char_split.split_compound(token._.text)  # type: ignore
    if len(splits) > 0:
        score, part1, part2 = splits[0]
        # It's not an actual probability, it can be lower than 0 and higher than 1
        if score > splitparam and len(part1.strip()) > 0 and len(part2.strip()) > 3:
            # Split the word, recursively find matches for the second part of the word
            part2 = part2.capitalize() if token.text.isupper() else part2
            token_ = copy(token.doc)[token.i]
            assert token_.text == token.text
            # TODO `part2` should also be lemmatized here, but perhaps not always due to efficiency:
            token_._.text = part2
            token_._.lemma_ = stuff.pipelines.lemmas(part2)[0].lemma_
            sentence_ = token_.sent
            sentence_._.text = tokens_to_string(
                [token_ if t.i == token.i else t for t in sentence_]
            )
            # Adjust the results to also include the first part of the word
            for match in matches_per_token(
                token_, gendering_type, stuff, splitparam, part1, recursion + 1
            ):
                suggestions += [r["value"] for r in match["replacements"]]
    return suggestions


def matches_per_token(
    token: Token,
    gendering_type: GenderingType,
    stuff: LargeNlpStuff,
    splitparam: float,
    token_prefix: str = "",
    recursion: int = 0,
) -> list[Match]:
    """
    Returns matches triggered by the word. The context is also considered, so multi-word rules also work.
    @param recursion: How deep should we should search for subwords.
    """
    lemma_: str = token._.lemma_  # type: ignore
    suggestions: list[str] = []
    if lemma_ in stuff.rules:
        # Find applicable rules and retrieve raw suggestions
        applicable_rules = [
            rule for rule in stuff.rules[lemma_] if is_applicable(rule, token)
        ]
        # Simplify the suggestions in some cases
        for rule in applicable_rules:
            rule.sensitive = simplify_participles(rule.sensitive, token)
        # Inflect the suggestions to match the replaced word in case and number
        inflected_rules = list(
            chain(
                *[
                    inflect_root(
                        token,
                        rule.sensitive,
                        rule.category_id,
                        rule.source,
                        gendering_type,
                        stuff,
                        token_prefix,
                    )
                    for rule in applicable_rules
                ]
            )
        )
        # Remove suggestions that don't change anything
        suggestions = [alt for alt in inflected_rules if token.text != alt]
    # Try subwordsplitting when there are not many results:
    if len(suggestions) < 5 and recursion <= 0:
        suggestions += matches_from_subtokens(
            token, gendering_type, stuff, splitparam, recursion
        )
    # Only return a match when there are some suggestions:
    if len(suggestions) > 0:
        return [
            gender_match(
                token.text,
                suggestions,
                token._.idx,  # type: ignore
                len(token.text),
            )
        ]
    return []
