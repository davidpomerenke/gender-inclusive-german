from typing import Optional, Dict

from .custom_types import (
    GenderingType,
    to_gendering_type,
)

TEXT_FIELD = "text"

GENDERING_TYPE_FIELD = "genderingType"

GENDERING_SYMBOL_FIELD = "genderSymbol"


def form_to_gendering_type(form: Dict[str, str]) -> Optional[GenderingType]:
    symbol = (
        form[GENDERING_SYMBOL_FIELD]
        if GENDERING_SYMBOL_FIELD in form and type(form[GENDERING_SYMBOL_FIELD]) == str
        else None
    )
    if GENDERING_TYPE_FIELD in form:
        return to_gendering_type(form[GENDERING_TYPE_FIELD], symbol)
    return None
