astroid==2.9.3
attrs==21.4.0
black==22.1.0
blis==0.7.5
catalogue==2.0.6
certifi==2021.10.8
charset-normalizer==2.0.11
click==8.0.3
compound-split==1.0.2
cymem==2.0.6
de-dep-news-trf==3.2.0
emoji==1.6.3
filelock==3.4.2
Flask==2.0.2
gunicorn==20.1.0
huggingface-hub==0.4.0
idna==3.3
iniconfig==1.1.1
isort==5.10.1
itsdangerous==2.0.1
Jinja2==3.0.3
joblib==1.1.0
langcodes==3.3.0
lazy-object-proxy==1.7.1
MarkupSafe==2.0.1
mccabe==0.6.1
murmurhash==1.0.6
mypy-extensions==0.4.3
nodeenv==1.6.0
numpy==1.22.2
packaging==21.3
pathspec==0.9.0
pathy==0.6.1
platformdirs==2.4.1
pluggy==1.0.0
preshed==3.0.6
protobuf==3.19.4
py==1.11.0
pydantic==1.8.2
pylint==2.12.2
pyparsing==3.0.7
pyright==0.0.13
pytest==7.0.0
PyYAML==6.0
regex==2022.1.18
requests==2.27.1
sacremoses==0.0.47
six==1.16.0
smart-open==5.2.1
spacy==3.2.1
spacy-alignments==0.8.4
spacy-legacy==3.0.8
spacy-loggers==1.0.1
spacy-stanza==1.0.1
spacy-transformers==1.1.4
srsly==2.4.2
stanza==1.3.0
thinc==8.0.13
tokenizers==0.10.3
toml==0.10.2
tomli==2.0.1
torch==1.10.2
tqdm==4.62.3
transformers==4.15.0
typer==0.4.0
typing_extensions==4.0.1
urllib3==1.26.8
wasabi==0.9.0
Werkzeug==2.0.3
wrapt==1.13.3
