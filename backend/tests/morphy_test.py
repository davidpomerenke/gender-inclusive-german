from ..src.app import stuff
from ..src.custom_types import Morph
from ..src.morphy.morphy import inflect


def test_morphy_inflect():
    assert "Lehrers" in inflect(
        "Lehrer", Morph("SUB", "Gen", "Sing", "Masc"), stuff.morphy
    )
    assert "Lehrerinnen" in inflect(
        "Lehrerin", Morph("SUB", "Nom", "Plur", "Fem"), stuff.morphy
    )
    assert "Lehrerinnen" in inflect(
        "Lehrerin", Morph("SUB", "Dat", "Plur", "Fem"), stuff.morphy
    )
