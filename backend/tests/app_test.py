import json

import pytest
from flask.testing import FlaskClient

from ..src.app import app


@pytest.fixture()
def client():
    return app.test_client()


def test_static_sites(client: FlaskClient):
    res = client.get("/")
    assert res.status == "200 OK"
    title = "<title>INCLUSIFY - einfach diversitätssensibel.</title>"
    assert title in res.data.decode("utf-8")  # type: ignore


def test_api(client: FlaskClient):
    res = client.post(
        "/v2/check",
        data={
            "text": "Die Bauarbeiter bauarbeiteten.",
            "language": "de-DE",
            "genderingType": "gender-symbol",
            "genderSymbol": "custom",
            "customGenderSymbol": "@",
        },
    )
    data = json.loads(res.data.decode("utf-8"))  # type: ignore
    assert len(data["matches"]) == 1
    assert len(data["matches"][0]["replacements"]) > 0
