import itertools as it
from typing import List

from ..src.app import stuff
from ..src.custom_types import DoubleNotation
from ..src.matches import LargeNlpStuff, matches, matches_per_sentence
from ..src.spacy_helpers import gender_symbol_wrapper

# from Wikipedia, "Beamter (Deutschland)"
sents = [
    "Ein Beamter in Deutschland (Bundes-, Landes-, Kommunalbeamter) steht gegenüber seinem Dienstherrn in einem öffentlich-rechtlichen Dienst- und Treueverhältnis.",
    "Beamte gehören nicht zu den Arbeitnehmern.",
    "Vom Beamtentum abzugrenzen sind daher die Beschäftigungsverhältnisse als Arbeitnehmer im Öffentlichen Dienst (Tarifbeschäftigte), die sich nach Arbeitsrecht und Tarifverträgen richten.",
]

sents_ = " ".join(sents)

words = [
    ["Beamter", "Kommunalbeamter", "Dienstherrn"],
    ["Beamte", "Arbeitnehmern"],
    ["Arbeitnehmer"],
]

words_ = list(it.chain(*words))

# Helpers:


nlp = gender_symbol_wrapper(stuff.pipelines, gendering_type=DoubleNotation())
stuff_ = LargeNlpStuff(nlp, stuff.morphy, stuff.rules)


def match_words_by_position(text: str) -> List[str]:
    return [
        text[m["offset"] : (m["offset"] + m["length"])] for m in matches(text, stuff_)
    ]


def match_words_by_text(text: str) -> List[str]:
    return [m["context"]["text"] for m in matches(text, stuff_)]


# Tests:


def test_matches_are_correct():
    for s, w in zip(sents, words):
        assert match_words_by_text(s) == w
    assert match_words_by_text(sents_) == words_


def test_match_positions_are_coherent():
    for s in sents:
        assert match_words_by_text(s) == match_words_by_position(s)
    assert match_words_by_text(sents_) == match_words_by_position(sents_)


def test_sentences_are_cached():
    matches_per_sentence.cache_clear()
    ci = matches_per_sentence.cache_info
    assert ci().currsize == 0
    matches(sents_, stuff_)
    assert ci().currsize == len(sents)
    matches(sents_, stuff_)
    assert ci().currsize == len(sents)
    sents__ = " ".join(
        [
            sents[0],
            "Hier kommt ein Zwischensatz;",
            "Beamte gehören nicht zu den Arbeitgebern.",
            *sents[2:],
        ]
    )
    matches(sents__, stuff_)
    assert ci().currsize == len(sents) + 2
    assert ci().misses == len(sents) + 2
    assert ci().hits == len(sents) + len(sents) - 1


def test_sentence_segmentation():
    with_spaces = " ".join(sents)
    with_breaks = "\n".join(sents)
    no_punct_with_breaks = "\n\n".join([s[:-1] for s in sents])
    no_punct_with_one_break = "\n".join([s[:-1] for s in sents])
    assert len(list(nlp.sents(with_spaces).sents)) == len(sents)
    assert len(list(nlp.sents(with_breaks).sents)) == len(sents)
    assert len(list(nlp.sents(no_punct_with_breaks).sents)) == len(sents)
    assert not len(list(nlp.sents(no_punct_with_one_break).sents)) == len(sents)


subword_sents = [
    "Ein Belustigungsbeamter in Deutschland (Bundes-, Landes-, Kommunalbeamter) steht gegenüber seinem Dienstherrn in einem öffentlich-rechtlichen Dienst- und Treueverhältnis.",
    "Superbeamte gehören nicht zu den Normalarbeitnehmern.",
    "Vom Beamtentum abzugrenzen sind daher die Beschäftigungsverhältnisse als Unsinnsarbeitnehmer im Öffentlichen Dienst (Tarifbeschäftigte), die sich nach Arbeitsrecht und Tarifverträgen richten.",
]

subword_words = [
    ["Belustigungsbeamter", "Kommunalbeamter", "Dienstherrn"],
    ["Superbeamte", "Normalarbeitnehmern"],
    ["Unsinnsarbeitnehmer"],
]


def test_detect_subword_matches():

    for s, w in zip(subword_sents, subword_words):
        assert match_words_by_text(s) == w
