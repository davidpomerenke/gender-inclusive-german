import json
from pathlib import Path
import subprocess

from ..src.benchmark import benchmark, exponential_score, fmt, run_benchmark


def dumb_system(sentence: str, uses_gender_symbols: bool):
    return [], [sentence]


def test_benchmark_dumb_system():
    metrics = benchmark(dumb_system)
    N = 100
    assert metrics["tp"] == 0
    assert N * 0.4 < metrics["fn"] < N * 1.5
    assert metrics["fp"] == 0
    assert N * 5 < metrics["tn"] < N * 50
    assert metrics["recall"] == 0
    assert metrics["precision"] == 0
    assert metrics["f1"] == 0
    assert metrics["score (exponential)"] == 0


benchmark_data = [
    {
        "x": "Bitte die Hundehalter und Hunde nicht beißen.",
        "y": "Bitte die Hundehaltenden und Hünd*innen nicht beißen.",
        "matches": ["Hundehalter", "Hunde"],
        "symbol_matches": 500,
    }
]


def test_benchmark_example_dumb():
    metrics = benchmark(dumb_system, benchmark_data)
    assert metrics["f1"] == 0
    assert metrics["score (exponential)"] == 0


def test_benchmark_example_a():
    def mock_system_a(sentences: str, use_gender_symbols: bool):
        # perfect system
        return ["Hunde", "Hundehalter"], [
            "Bitte die Hundehaltenden und Hünd*innen nicht beißen.",
            "Bitte die Hundehalter*innen und Hünd*innen nicht beißen.",
        ]

    metrics = benchmark(mock_system_a, benchmark_data)
    assert metrics["f1"] == 1.000
    assert metrics["score (exponential)"] == 1.000
    assert metrics["score (exponential)"] == exponential_score(0)


def test_benchmark_example_b():
    def mock_system_b(sentences: str, use_gender_symbols: bool):
        # misses 1 of 2 labels
        return ["Hundehalter"], [
            "Bitte die Hundehaltenden und Hunde nicht beißen.",
            "Bitte die Hundehalter*innen und Hunde nicht beißen.",
        ]

    metrics = benchmark(mock_system_b, benchmark_data)
    assert metrics["recall"] == 0.5
    assert metrics["precision"] == 1.000
    assert metrics["score (exponential)"] == 0.000


def test_benchmark_example_b1():
    def mock_system_b1(sentences: str, use_gender_symbols: bool):
        # misses 1 of 2 labels but magically knows the right target sentence
        return ["Hundehalter"], [
            "Bitte die Hundehaltenden und Hünd*innen nicht beißen.",
            "Bitte die Hundehalter*innen und Hünd*innen nicht beißen.",
        ]

    metrics = benchmark(mock_system_b1, benchmark_data)
    assert metrics["recall"] == 0.5
    assert metrics["precision"] == 1.000
    # sentences with mismatched labels do not influence the score:
    assert metrics["score (exponential)"] == 0.000


def test_benchmark_example_c():
    def mock_system_c(sentences: str, use_gender_symbols: bool):
        # gives a false positive
        return ["Bitte"], [
            "Bitter die Hundehalter und Hunde nicht beißen.",
        ]

    metrics = benchmark(mock_system_c, benchmark_data)
    assert metrics["recall"] == 0.000
    assert metrics["precision"] == 0.000
    assert metrics["score (exponential)"] == 0.000


def test_benchmark_multiples():
    benchmark_data = [
        {
            "x": "a b c",
            "y": "a b d e",
            "matches": ["c"],
            "symbol_matches": 0,
        },
        {
            "x": "x y z",
            "y": "x y v w",
            "matches": ["z"],
            "symbol_matches": 0,
        },
    ]

    def mock_system_a(sentence: str, use_gender_symbols: bool):
        # mislabels one sentence, gets other sentence right in 3rd ranked option
        if sentence == "a b c":
            return ["b"], ["a d c"]
        else:  # if sentence == "x y z":
            return ["z"], ["x y t u", "x y q r", "x y v w", "x y v w"]

    metrics = benchmark(mock_system_a, benchmark_data)
    assert metrics["recall"] == 0.5
    assert metrics["precision"] == 0.5
    # only the well-labeled sentences affect the score
    assert metrics["score (exponential)"] == fmt(exponential_score(2))


def test_no_regression():
    run_benchmark()
    s = subprocess.run(
        ["git", "show", "HEAD:backend/evaluation.json"],
        cwd=Path(__file__).parent,
        capture_output=True,
    )
    previous = json.loads(s.stdout.decode("utf-8"))
    with open(Path(__file__).parent / ".." / "evaluation.json") as f:
        current = json.load(f)
    assert current["score (logistic)"] >= previous["score (logistic)"]
    assert current["f1"] >= previous["f1"]
    assert current["recall"] >= previous["recall"]
    assert current["precision"] >= previous["precision"]
