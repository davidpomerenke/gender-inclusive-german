from ..src.app import stuff
from ..src.custom_types import GenderingSymbol
from ..src.spacy_helpers import (
    remove_gender_symbols,
    restore_gender_symbols,
    gender_symbol_wrapper,
)

sents = [
    "Die  _Bundeskanzler/-in_ am Abend *in Berlin*: 'Liebe Freund:innen, Römer_innen und Landsleute*! Als Wissen- schaftler...in",
    "Die Ameis*innen sind schläfrig.",
]


def test_remove_gender_symbols():
    sent = sents[0]
    sent_, _ = remove_gender_symbols(sent, gender_symbols=["..."])
    assert (
        sent_
        == "Die  _Bundeskanzlerin_ am Abend *in Berlin*: 'Liebe Freundinnen, Römerinnen und Landsleute*! Als Wissen- schaftlerin"
    )


def test_restore_gender_symbols():
    for sent in sents:
        sent_, symbols = remove_gender_symbols(sent, gender_symbols=["..."])
        assert sent_ != sent
        sent__ = restore_gender_symbols(sent_, symbols)
        assert sent__ == sent


def test_restore_gender_symbols_stanza():
    for sent in sents:
        # `load_language_models` has built-in gender symbol removal and restoration.
        # The restoration happens in the doc._ namespace, because the original
        # attributes cannot be modified.
        nlp = gender_symbol_wrapper(
            stuff.pipelines, gendering_type=GenderingSymbol("...")
        )
        doc = nlp.full(sent)
        assert doc._.text == sent  # type: ignore
