from ..src.custom_types import GenderingSymbol
from ..src.grammar_transfer import inflect_root
from ..src.app import stuff

examples = [
    # schema: [category_id, old, new, inflected]
    (0, "dem Lehrer", "Lehrerin", "Lehrerin"),
    (0, "den Lehrern", "Lehrerin", "Lehrerinnen"),
    (0, "die Lehrer", "Lehrkraft", "Lehrkräfte"),
    (0, "den Lehrern", "Lehrkraft", "Lehrkräften"),
    (1, "die Lehrer", "Baum", "Bäume"),
    (1, "den Lehrern", "Baum", "Bäumen"),
]


def test_inflect_root():
    for category, old, new, inflected in examples:
        old_token = stuff.pipelines.full(old)[1]
        inflected_ = inflect_root(
            old_token, new, category, "random source", GenderingSymbol("*"), stuff, ""
        )
        print(old, new)
        assert inflected in inflected_
