from ast import literal_eval
from pathlib import Path
import subprocess


def test_cli():
    s = subprocess.run(
        ["python", "-m", "src.cli", "--text", "Ich bin Informatiker."],
        cwd=Path(__file__).parent.parent,
        capture_output=True,
    )
    out = literal_eval(s.stdout.decode("utf-8"))
    assert len(out["replacements"]) > 0
