import { resolve } from "path";

const repoRoot = resolve(__dirname, "..", "..");

const dataDir = resolve(repoRoot, "data");
const reactUiDir = resolve(repoRoot, "frontend");
const serverDir = resolve(repoRoot, "backend");

export { repoRoot, dataDir, reactUiDir, serverDir };
