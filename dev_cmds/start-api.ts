import { execPiped, runAsyncMain } from "devcmd";
import { repoRoot } from "./utils/paths";
import { DEVCMD_COMMAND } from "./utils/commands";

async function main() {
  await execPiped({ command: DEVCMD_COMMAND, args: ["prepare-server"] });
  await execPiped({
    command: "bash",
    args: [
      "-c",
      [
        "source $(conda info --base)/etc/profile.d/conda.sh",
        "conda activate inclusify",
        "pip install -r backend/requirements.in",
        "gunicorn --chdir backend src.app:app --bind localhost:8081 --timeout 90",
      ].join(" && "),
    ],
    options: {
      cwd: repoRoot,
    },
  });
}

runAsyncMain(main);
